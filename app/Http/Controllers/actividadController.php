<?php

namespace App\Http\Controllers;
use App\actividadModel;
use Illuminate\Http\Request;
use View;
use Redirect;
use Illuminate\Support\Facades\Input;

class actividadController extends Controller
{
    public function index(){
        $actividad = actividadModel::all();
        return view('actividad.index', compact('actividad'));
    }

    public function create() {
        $actividad = new actividadModel();
        return view('actividad.create', $actividad);
    }

    public function store(Request $request){
        try {
            $actividad = new actividadModel();
            $actividad->descripcion = Input::get('descripcion');
            $actividad->save();
            return redirect('actividad');
        } catch (\Throwable $th) {
            return Redirect::to('actividad');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        actividadModel::find($id)->delete();
        return redirect('actividad');
    }

    public function edit($id){
        $actividad = actividadModel::find($id);
        return View::make('actividad.edit', compact('actividad'));
    }

    public function update(Request $request, $id){
        try {
            $actividad = actividadModel::find($id);
            $actividad->descripcion = $request->descripcion;
            $actividad->save();
            return Redirect::to('actividad');
        } catch (\Throwable $th) {
            return Redirect::to('actividad');
        }
    }

}
