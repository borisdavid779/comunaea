<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\comuneroModel;
use View;
use Barryvdh\DomPDF\Facade as PDF;
use Session;
use Redirect;
use Illuminate\Support\Facades\Input;
class comuneroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comunero = comuneroModel::all();
        return view('comunero.index', compact('comunero'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $comunero = new comuneroModel();
        return View::make('comunero.create', compact('comunero'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [ 
            'cedula' => 'unique:comunero|numeric', 
            ]
        );
        try {
            $comunero = new comuneroModel();
            $comunero->cedula = Input::get('cedula');
            $comunero->nombres = Input::get('nombres');
            $comunero->apellidos = Input::get('apellidos');
            $comunero->fecha_nacimiento = Input::get('fecha_nacimiento');
            $comunero->direccion = Input::get('direccion');
            $comunero->telefono = Input::get('telefono');
            $comunero->save();
            Session::flash('message','Se registro correctamente');
        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('message', 'No se puedo registrar');
        }
        return redirect('comunero');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comunero = comuneroModel::find($id);
        return View::make('comunero.update', compact('comunero'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $comunero = comuneroModel::find($id);
            $comunero->nombres = $request->nombres;
            $comunero->apellidos = $request->apellidos;
            $comunero->cedula = $request->cedula;
            $comunero->direccion = $request->direccion;
            $comunero->fecha_nacimiento = $request->fecha_nacimiento;
            $comunero->telefono = $request->telefono;
            $comunero->save();
            return Redirect::to('comunero');
        } catch (\Throwable $th) {
            return Redirect::to('comunero');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        comuneroModel::find($id)->delete();
        return redirect('comunero');
    }

    public function imprimir(){
        $pdf = PDF::loadView('comunero.list');
        $pdf->setPaper('A4');
        return $pdf->stream();
    }
    
    public function infouser (Request $id){
        $comunero = comuneroModel::find($id);
        dd("jajaj");
        //$vista = view('infouser')->with('infouser', $comunero);
        //$pdf =  \PDF::loadHTML($vista);
        //return $pdf->stream('comunero.infouser');
    }
}