<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Comuna Eloy Alfaro</title>
        <!-- Fonts --> 
        <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet"> 
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
        
    </head>
    <body>
        <div>
           <nav>
                <ul>
                    <li><a class="def" href="#inicio">Inicio</a></li>
                    <li><a href="#objetivo">Misión</a></li>
                    <li><a href="#objetivo">Visión</a></li>
                    <li><a href="#sobrenoso">Sobre nosotros</a></li>
                    <li><a href="#">Actividades comerciales</a></li>
                    <li><a href="#">Más Información</a>
                        <ul class="submenu">
                            <li><a href="">Actión</a></li>
                            <li><a href="">Atother action</a></li>
                            <li><a href="">Something else here</a></li>
                        </ul>
                    </li> 
                    <div class="acceso">  
                    @if (Route::has('login')) 
                        @auth
                            <li class="item-r"><a a class="def" href="{{ url('/home') }}">Ciudadano digital</a></li>
                            @else
                                <li class="item-r"><a class="def" href="{{ route('login') }}">Ingresar</a></li>
                                <li class="item-r"><a class="def" href="{{ route('register') }}">Registrarme</a></li>            
                        @endauth
                    @endif
                </div>
                </ul>
            </nav>
            
            <header>
                <section class="textos-header">
                    <h1>COMUNA ELOY ALFARO</h1>
                </section>
                
                <div class="wave" style="height: 150px; overflow: hidden;">
                    <svg viewBox="0 0 500 150" preserveAspectRatio="none" style="height: 100%; width: 100%;">
                        <path d="M0.00,49.98 C150.00,150.00 349.20,-50.00 500.00,49.98 L500.00,150.00 L0.00,150.00 Z" style="stroke: none; fill: #fff;"></path>
                    </svg>
                </div>
            </header>
        </div>
        <main>
            <section class="contenedor sobre-nosotros" id = "objetivo">
                <h2 class="titulo">Para donde vamos</h2>
                <div class="contenedor-sobre-nosotros">
                    <img src="img/ilustracion2.svg" class="imagen-about-us">
                    <div class="contenido-textos">
                        <h3><span>1</span>MISIÓN</h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt veniam eius aspernatur ad
                            consequuntur aperiam minima sed dicta odit numquam sapiente quam eum, architecto animi pariatur,
                            velit doloribus laboriosam ut.</p>
                        <h3><span>2</span>VISIÓN</h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt veniam eius aspernatur ad
                            consequuntur aperiam minima sed dicta odit numquam sapiente quam eum, architecto animi pariatur,
                            velit doloribus laboriosam ut.</p>
                    </div>
                </div>
            </section>

            <section class="about-services" id = "sobrenoso">
                <div class="contenedor">
                    <h2 class="titulo">Nuestros servicios</h2>
                    <div class="servicio-cont">
                        <div class="servicio-ind">
                            <img src="img/ilustracion1.svg" alt="">
                            <h3>¿Qué puedo encontrar aquí?</h3>
                            <p>Esta es una pagina en la que puedes encontrar información de los juegos más pupulares, que te
                            animarán a descargarlos y jugarlos.</p>
                        </div>
                        <div class="servicio-ind">
                            <img src="img/imagen1.png" alt="" >
                            <h3>Name</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore, qui?</p>
                        </div>
                        <div class="servicio-ind">
                            <img src="img/imagen3.png" alt="">
                            <h3>¿Cómo funciona?</h3>
                            <p>Solo tienes que dar click sobre una de las imagenes del juego que te llame la atención y te
                            llevará a videos que puedes visualizar para informate sobre él.</p>
                        </div>
                    </div>
                </div>
            </section>

            <section class="clientes contenedor">
                <h2 class="titulo">Nuestros integrantes</h2>
                <div class="cards">
                    <div class="card">
                        <img src="img/face1.jpg" alt="">
                        <div class="contenido-texto-card">
                            <h4>Name</h4>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae, sapiente!</p>
                        </div>
                    </div>
                    <div class="card">
                        <img src="img/face2.jpg" alt="">
                        <div class="contenido-texto-card">
                            <h4>Name</h4>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae, sapiente!</p>
                        </div>
                    </div>
                </div>
            </section>
        </main>

        <footer>
            <div class="contenedor-footer">
                <div class="content-foo">
                    <h4>Celular</h4>
                    <p>8296312</p>
                </div>
                <div class="content-foo">
                    <h4>Email</h4>
                    <p>8296312</p>
                </div>
                <div class="content-foo">
                    <h4>Ubicación</h4>
                    <p>8296312</p>
                </div>
            </div>
            <h2 class="titulo-final">&copy; Presidente de la Comuna | Ing. Edison Anchundia</h2>
        </footer> 

        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>    

    </body>
</html>
