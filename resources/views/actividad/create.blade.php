<div class="modal fade bd-example-modal-xl" id="create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h1>Registro de actividad</h1>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        {!! Form::open(['route'=>'actividad.store', 'type'=>'hidden', 'value'=>'{{csrf_token()}}', 'method'=>'POST', 'files'=>'true']) !!}
        <div class="modal-body">
            <label for="cargo">{{__('Descripción')}}</label>
            <textarea id="descripcion" type="text" name="descripcion" class="form-control{{ $errors->has('cargo') ? ' is-invalid' : '' }}" cols="20" rows="4" required autofocus></textarea>
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('cargo') }}</strong>
            </span>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">{{ __('Registrar') }}</button>
        </div>
        {!! Form::close() !!}
    </div>
    </div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

