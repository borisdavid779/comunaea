@extends('layouts.app')
@section('title', 'Actualizar cancion')
@section('content')
<div class="container" style="background-color:  #99a3a4;">
    <br>
    <h2 class="title">Actualización de datos actividad</h2>
    <hr>
    {!! Form::model($actividad, array('method' => 'PATCH', 'route' => array('actividad.update', $actividad->id))) !!}   
        <div class="col-md-6">
            <div class="form-group">
                <label for="cargo">{{__('Descripcion')}}</label>
                {!! Form::Textarea('descripcion',null,['class'=>'form-control']) !!}
            </div>
            {!!Form::submit('Actualizar datos',['class'=>'btn btn-primary'])!!}
        </div>  
    {!! Form::close() !!}
    <br>
</div>
@endsection