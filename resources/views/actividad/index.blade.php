@extends('layouts.app')
@section('content')
<div class="container" style="background-color:  #99a3a4;">
    <br>
    <div class="btn-group" role="group">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create">Registrar actividad <i class="far fa-plus-square"></i></button>
    </div>
    <h1>Actividades</h1>
    <hr>
    <div>
        @foreach ($actividad as $item)
            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <!--<h5 class="mb-1">List group item heading</h5>-->
                        <small>{{ $item->created_at }}</small>
                        <div class="d-flex justify-content-between">
                            <div class="p-1">
                                <button type="button" class="btn btn-outline-danger" data-target="#delete" data-toggle="modal" title="Eliminar">
                                <i class="fas fa-trash-alt"></i></button>
                            </div>
                            <div class="p-1">
                                {!! Form::open(array('method' => 'GET', 'route' => array('actividad.edit', $item->id))) !!}  
                                    <button type="submit" class="btn btn-outline-primary" data-toggle="tooltip"  title="Editar">
                                        <i class="fas fa-edit"></i></button>
                                {!! Form::close() !!} 
                            </div> 
                        </div>
                    </div>
                    <p class="mb-1">{{ $item->descripcion }}</p>
                    <small>Identificador: {{ $item->id }}</small>
                </a>
            </div>
            <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title">Confirmación</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                    <p><strong>¿Seguro que quieres eliminar la actividad</strong></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        {!! Form::open(['route' => ['actividad.destroy', $item->id], 'method' => 'DELETE']) !!}   
                            <button type="submit" class="btn btn-danger"  data-toggle="tooltip" title="Eliminar">
                            Eliminar</button>
                        {!! Form::close() !!}
                    </div>
                </div>
                </div>
            </div>
        @endforeach
    </div>
    @include('actividad.create')
    <br>
</div>
@endsection