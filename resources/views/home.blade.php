@extends('layouts.app')
@section('content')
<div class="container">
    <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
        <a type="button" href="{{ route('actividad.index') }}">
            <button type="button" class="btn btn-primary">Actividades <i class="fas fa-list-ul"></i></button>
        </a>    
        <a type="button" href="{{ route('actividad.index') }}">
            <button type="button" class="btn btn-primary">Solicitudes <i class="fas fa-align-center"></i></button>
        </a>  
        <a type="button" href="{{ route('comunero.index') }}">
            <button type="button" class="btn btn-primary">Comuneros <i class="fas fa-users"></i></button>
        </a>
        <a type="button" href="{{ route('asistencia.index') }}">
            <button type="button" class="btn btn-primary">Asistencia <i class="fas fa-tasks"></i></button>
        </a>
        <div class="btn-group" role="group">
            <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
            </button>
            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <a class="dropdown-item" href="#">Dropdown link</a>
                <a class="dropdown-item" href="#">Dropdown link</a>
            </div>
        </div>
    </div>
    <hr>
</div>
@endsection
