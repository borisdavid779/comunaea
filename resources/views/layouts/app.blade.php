<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ 'Comuna Eloy Alfaro' }}</title>
    <!--Fontawesome CDN-->
    <link rel="stylesheet" href="{{ asset('https://use.fontawesome.com/releases/v5.9.0/css/all.css') }}" integrity="sha384-i1LQnF23gykqWXg6jxC2ZbCbUMxyw5gLZY6UiUS98LYV5unm8GWmfkIS6jqJfb4E" crossorigin="anonymous">
    <!-- Styles 
    <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ 'Comuna Eloy Alfaro' }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav ml-md-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Acceder</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('register') }}">Registrarme</a></li>
                @else
                    <li class="nav dropdown">
                        <a a id="navbarDropdown" href="#" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            @if (Auth::user()->tipo_users_id == 1)
                                Administrador:
                            @else
                                @if (Auth::user()->tipo_users_id == 2)
                                    Secretaria:
                                @else
                                    Oficial:
                                @endif
                            @endif
                            {{ Auth::user()->name }} <span class="caret"></span>
                            <i class="fa fa-user-circle"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Cerrar sesión
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </nav>
        <div class="container">
            <br>
            <a href="javascript:history.back()"> 
                <i class="fas fa-arrow-left"></i>
                Volver Atrás
            </a>
        </div>
        <br>
        @yield('content')
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
    $('#update').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var nombres = button.data('nombres') 
        var apellidos = button.data('apellidos') 
        var cedula = button.data('cedula') 
        var direccion = button.data('direccion') 
        var fecha_nacimiento = button.data('fecha_nacimiento') 
        var telefono = button.data('telefono') 
        var modal = $(this)
        modal.find('.modal-body #nombres').val(nombres)
        modal.find('.modal-body #apellidos').val(apellidos)
        modal.find('.modal-body #cedula').val(cedula)
        modal.find('.modal-body #direccion').val(direccion)
        modal.find('.modal-body #fecha_nacimiento').val(fecha_nacimiento)
        modal.find('.modal-body #telefono').val(telefono)
    })
    </script>
</body>
</html>