@extends('layouts.app')
@section('content')
<div class="container">
        <div class="btn-group" role="group">
            <a type="button" href="{{ route('comunero.create') }}">
                <button type="button" class="btn btn-primary">Registrar comunero <i class="far fa-plus-square"></i></button>
            </a>
            <a href="{{ url('list') }}" data-toggle="tooltip" target="_blank"> 
                <button type="button" class="btn btn-danger">Reporte Comuneros <i class="far fa-file-pdf"></i></button>    
            </a>  
        </div>
        <h1>Comuneros</h1>
        @if (Session::has('notice'))
        <div class="container">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ Session::get('notice') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
        <hr>
        <div>
            @foreach ($comunero as $item)
            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">{{ $item->id }}. <strong>{{ $item->apellidos }} {{ $item->nombres }}</strong></h5>
                        <small>Fehca nacimiento: <strong>{{ $item->fecha_nacimiento }}</strong></small>
                        <div class="d-flex justify-content-between">
                            <div class="p-1">
                                <button type="button" class="btn btn-outline-danger" data-target="#delete" data-toggle="modal" title="Eliminar">
                                <i class="fas fa-trash-alt"></i></button>
                            </div>
                            <div class="p-1">
                                {!! Form::open(array('method' => 'GET', 'route' => array('comunero.edit', $item->id))) !!}   
                                    <button type="submit" class="btn btn-outline-primary" data-toggle="tooltip"  title="Editar">
                                    <i class="fas fa-user-edit"></i></button>
                                {!! Form::close() !!} 
                            </div>
                            <div class="p-1">
                                Info
                            </div>
                        </div>
                    </div>
                    <p class="mb-1">Dirección: {{ $item->direccion }}</p>
                    <small>Cedula: <strong>{{ $item->cedula }}</strong></small>
                    <small>Teléfono: <strong>{{ $item->telefono }}</strong></small>
                </a>
            </div>
            <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Confirmación</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <p><strong>¿Seguro que quieres eliminar al comunero</strong> {{ $item->nombres }} {{ $item->apellidos }} <strong>?</strong></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        {!! Form::open(['route' => ['comunero.destroy', $item->id], 'method' => 'DELETE']) !!}   
                            <button type="submit" class="btn btn-danger"  data-toggle="tooltip" title="Eliminar">
                            Eliminar</button>
                        {!! Form::close() !!}
                    </div>
                  </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@endsection