<div class="form-row">
    <div class="form-group col-md-6">
        <label for="cargo">{{__('Nombres')}}</label>
        <input id="nombres" type="text" name="nombres" class="form-control{{ $errors->has('nombres') ? ' is-invalid' : '' }}" required autofocus/>
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('nombres') }}</strong>
        </span>
    </div>      
    <div class="form-group col-md-6">
        <label for="apellidos">{{ __('Apellidos') }}</label>
        <input type="text" class="form-control" name="apellidos" id="apellidos" value="{{ old('apellidos') }}" required autofocus/>
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('apellidos') }}</strong>
        </span>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="cedula">{{__('Cédula')}}</label>
        <input id="cedula" type="number" class="form-control{{ $errors->has('cedula') ? ' is-invalid' : '' }}" name="cedula" value="{{ old('cedula') }}" required autofocus>
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('cedula') }}</strong>
        </span>
    </div>
    <div class="form-group col-md-6">
        <label for="direccion">{{__('Dirección')}}</label>
        <textarea id="direccion" type="text" class="form-control{{ $errors->has('cedula') ? ' is-invalid' : '' }}" name="direccion" value="{{ old('direccion') }}" cols="20" rows="4"  required autofocus></textarea>
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('direccion') }}</strong>
        </span>
    </div>
</div>
<div class="form-group row">
    <div class="form-group col-md-6">
        <label for="fecha_nacimiento">{{__("Fecha nacimiento")}}</label>
        <input type="date" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" value="{{ old('fecha_nacimiento') }}" required autofocus/>
    </div>
    <div class="form-group col-md-6">
        <label for="telefono">{{__('Teléfono')}}</label>
        <input id="telefono" type="number" class="form-control{{ $errors->has('telefono') ? ' is-invalid' : '' }}" name="telefono" value="{{ old('telefono') }}" required autofocus>
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('telefono') }}</strong>
        </span>
    </div>
</div>