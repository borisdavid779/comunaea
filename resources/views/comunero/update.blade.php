@extends('layouts.app')
@section('title', 'Actualizar cancion')
@section('content')
<div class="container" style="background-color:  #99a3a4;">
    <br>
    <h2 class="title">Actualización de datos comunero</h2>
    <hr>
    {!! Form::model($comunero, array('method' => 'PATCH', 'route' => array('comunero.update', $comunero->id))) !!}   
        @include('comunero.edit')
        <div class="col-md-6 offset-md-5">
            {!!Form::submit('Actualizar datos',['class'=>'btn btn-primary'])!!}
        </div>  
    {!! Form::close() !!}
    <br>
</div>
@endsection