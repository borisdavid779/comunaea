@extends('layouts.app')
@section('content')
    <div class="container" style="background-color:  #99a3a4 ;">
        <br>    
        <h1>Registro de comunero</h1>
        <hr>
        {!! Form::open(['route'=>'comunero.store', 'type'=>'hidden', 'value'=>'{{csrf_token()}}', 'method'=>'POST', 'files'=>'true']) !!}
            @include('comunero.input')
            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-5">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Registrar') }}
                    </button>
                </div>                         
            </div>
        {!! Form::close() !!}
        <br>
    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="{{asset('bootstrap/js/bootstrap-datepicker.js')}}"></script>
    <!-- Languaje -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script src="{{asset('js\bootstrap-datepicker.es.min.js')}}"></script>
    <script text="text/javascript">
        $('.datepicker').datepicker({
            format: "yy/mm/dd",
            language: "es",
            autoclose: true
        });
    </script>
@endsection