<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="{{ asset('public/favicon.ico') }}"/>
    <link rel="stylesheet" href="{{ asset('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css') }}">
    <title>Comunero</title>
</head>
<body>
    <div class="container-fluid">
        <h3>Comuna Eloy Alfaro</h3>
        <h6>Listado de Comuneros</h6>
        <small>Generado a las: {{ date('H:i Y/m/d') }}</small>
        <hr class="my-2">
        <ul class="list-unstyled">
            @foreach ($comunero as $item)
                <div class="alert alert-light" role="alert">
                    <strong>{{ $item->nombres }} {{$item->apellidos }}</strong>
                    <h6>Cédula: {{ $item->cedula }}</h6>
                    <h6>Telefono: {{ $item->telefono }}</h6>
                    <h6>Dirección: {{ $item->direccion }}</h6>
                    <h6>Fecha Nacimiento: {{ $item->fecha_nacimiento }}</h6>
                </div>
                <hr class="my-2">
            @endforeach
          </ul>
    </div>
</body>
</html>