<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="{{ asset('public/favicon.ico') }}"/>
    <link rel="stylesheet" href="{{ asset('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css') }}">
    <title>Comunero</title>
</head>
<body>
    <div class="container-fluid">
        <h3>Comuna Eloy Alfaro</h3>
        <h6>Listado de Comuneros</h6>
        <small>Generado a las: {{ date('H:i Y/m/d') }}</small>
        <table class="table table-sm">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido</th>
                <th scope="col">Cedula</th>
                <th scope="col">Dirección</th>
                <th scope="col">Fecha de Nacimiento</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($comunero as $item)
              <tr>
                <th scope="row">{{ $item->id }}</th>
                <td>{{ $item->nombres }}</td>
                <td>{{$item->apellidos }}</td>
                <td>{{ $item->cedula }}</td>
                <td>{{ $item->direccion }}</td>
                <td>{{ $item->fecha_nacimiento }}</td>
              </tr>
            @endforeach
            </tbody>
          </table>
        <hr class="my-1">
    </div>
</body>
</html>