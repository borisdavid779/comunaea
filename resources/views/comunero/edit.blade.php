<div class="form-row">
    <div class="form-group col-md-6">
        <label for="cargo">{{__('Nombres')}}</label>
        {!! Form::Text('nombres',null,['class'=>'form-control', 'placeholder'=>'Ingrese nuevo titulo']) !!}
    </div>      
    <div class="form-group col-md-6">
        <label for="apellidos">{{ __('Apellidos') }}</label>
        {!! Form::Text('apellidos',null,['class'=>'form-control']) !!}
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="cedula">{{__('Cédula')}}</label>
        {!! Form::Text('cedula',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group col-md-6">
        <label for="direccion">{{__('Dirección')}}</label>
        {!! Form::Textarea('direccion',null,['class'=>'form-control', 'cols'=>'20', 'rows'=>'4']) !!}
    </div>
</div>
<div class="form-group row">
    <div class="form-group col-md-6">
        <label for="fecha_nacimiento">{{__("Fecha nacimiento")}}</label>
        {!! Form::date('fecha_nacimiento',null,['class'=>'form-control', 'required autofocus']) !!}
    </div>
    <div class="form-group col-md-6">
        <label for="telefono">{{__('Teléfono')}}</label>
        {!! Form::number('telefono',null,['class'=>'form-control', 'required autofocus']) !!}
    </div>
</div>